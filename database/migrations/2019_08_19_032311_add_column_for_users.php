<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnForUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('photo')->nullable();
            $table->string('phone')->nullable();
            $table->text('biodata')->nullable();
            $table->string('noid')->nullable();
            $table->string('nocertificate')->nullable();
            $table->string('yearcertificate')->nullable();
            $table->date('dateofbirth')->nullable();
            $table->enum('genre', ['L', 'P'])->nullable();
            $table->string('marital')->nullable();
            $table->text('address')->nullable();
            $table->string('province')->nullable();
            $table->string('city')->nullable();
            $table->string('leveledu')->nullable();
            $table->string('univedu')->nullable();
            $table->string('skill')->nullable();
            $table->string('organizer')->nullable();
            $table->string('cv')->nullable();
            $table->string('ijazah')->nullable();
            $table->string('linkportofolio')->nullable();
            $table->string('linkfacebook')->nullable();
            $table->string('linktwitter')->nullable();
            $table->string('linkinstagram')->nullable();
            $table->boolean('status')->default(true);
            $table->boolean('is_updateprofil')->default(false);
            $table->string('cattr')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

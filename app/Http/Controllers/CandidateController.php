<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\JobApplicant;
use App\JobList;
use App\Corporate;
use App\Province;

class CandidateController extends Controller
{
    public function profile() {
        return view('candidate.profile');
    }

    public function setupProfile() {
        return view('candidate.setup');
    }

    public function setupProfileCreate(Request $request)
    {
        try {
            $this->validate($request ,[  
                // 'noid' => 'required|integer|digits_between:16,16',
                'dateofbirth' => 'required|date',
                'genre' => 'required',
                'marital' => 'required',
                'address' => 'required',
                'province' => 'required',
                'city' => 'required',
                'leveledu' => 'required',
                'univedu' => 'required',
                // 'skill' => 'required',
                // 'organizer' => 'required',
                // 'cv' => 'required',
                // 'ijazah' => 'required',
                'linkportofolio' => 'required',
                'linkfacebook' => 'required',
                'linktwitter' => 'required',
                'linkinstagram' => 'required',
            ]);

            $data = User::find(auth()->user()->id);
            $data->noid = $request->noid;
            $data->dateofbirth = $request->dateofbirth;
            $data->genre = $request->genre;
            $data->marital = $request->marital;
            $data->address = $request->address;
            $data->province = $request->province;
            $data->city = $request->city;
            $data->leveledu = $request->leveledu;
            $data->univedu = $request->univedu;
            $data->skill = $request->skill;
            $data->organizer = $request->organizer;
            $data->cv = $request->cv;
            $data->ijazah = $request->ijazah;
            $data->linkportofolio = $request->linkportofolio;
            $data->linkfacebook = $request->linkfacebook;
            $data->linktwitter = $request->linktwitter;
            $data->linkinstagram = $request->linkinstagram;
            $data->is_updateprofile = true;
            $data->save();
            return redirect('/');
        } catch (\Throwable $th) {
            return redirect()->back()->withInput()->withErrors($th);
        }
    }
    
    public function jobsApplicant() {
        $datas = JobApplicant::where('id_tr', auth()->user()->id)->orderBy('id_apl', 'DESC')->get();
        return view('candidate.jobs-application', compact('datas'));
    }

    public function jobsLists(Request $request) {
        $provinces = Province::where('status', 'Aktif')->orderBy('nm_prov', 'ASC')->get();

        $per_page = 10;

        $wheres = [];
        if ($request->province) $wheres = ['prov' => $request->province];
        if ($request->company_type) $wheres = ['type_cp' => $request->company_type];
        if ($request->job_type) $wheres = ['type_job' => $request->job_type];

        $datas = JobList::where($wheres);
        if ($request->job_name) {
            $datas = $datas->where('nm_job', 'LIKE', '%'.$request->job_name.'%');
        }        
        $datas = $datas->where('status_job', 'Publish')->orderBy('mulai', 'DESC')->paginate($per_page);

        if ($request->ajax()) {
            return view('candidate.jobs-list-ajax', compact('datas'));
        }
  
        return view('candidate.jobs-list', compact('datas', 'provinces'));
    }

    public function jobDetail($id)
    {
        $datas = JobList::where('id_job', $id)->first();
        
        $wheres_similiars = [
            'status_job' => 'Publish'
        ];
        $similiars = JobList::where($wheres_similiars)->inRandomOrder()->limit(4)->get();
        return view('candidate.job-description', compact('datas', 'similiars'));
    }

    public function applyJob($id)
    {
        $job = JobList::where('id_job', $id)->first();
        if (empty($job)) {
            return redirect()->back()->with('error', 'Code Job has not found.');
        }

        $wheres = [
            'id_tr' => auth()->user()->id,
            'id_job' => $id,
            'status' => 'Submitted'
        ];
        $datas = JobApplicant::where($wheres)->first();
        if (empty($datas)) {
            $datas = new JobApplicant;
            $datas->id_tr = auth()->user()->id;
            $datas->nm_tr = auth()->user()->name;
            $datas->jkel_tr = auth()->user()->genre;
            $datas->skema = auth()->user()->skill;
            $datas->pdklv_tr = auth()->user()->pdklv_tr;
            $datas->jur_tr = auth()->user()->jur_tr;
            $datas->gpa_tr = auth()->user()->gpa_tr;
            $datas->id_job = $id;
            $datas->nm_job = $job->nm_job;
            $datas->id_cp = $job->id_cp;
            $datas->nm_cp = $job->nm_cp;
            $datas->status = 'Submited';
            $datas->tglapl = \Carbon\Carbon::now();
            $datas->tahun = \Carbon\Carbon::now()->format('Y');
            $datas->save();
            return redirect()->back()->with('success', 'Successfull apply a job.');
        }
    }

    public function findCompany() {
        $datas = Corporate::orderBy('nm_cp', 'ASC')->get();
        return view('candidate.find-company', compact('datas'));
    }

    public function detailCompany($id) {
        $datas = Corporate::where('id_cp', $id)->first();
        return view('candidate.detail-company', compact('datas'));
    }
    
    public function changePassword() {
        return view('candidate.change-password');
    }
}

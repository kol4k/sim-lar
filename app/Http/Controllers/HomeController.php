<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\User;
use App\JobApplicant;
use App\JobList;
use App\Corporate;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $per_page = 5;
        $datas = JobList::where('status_job', 'Publish')->orderBy('mulai', 'DESC')->paginate($per_page);

        if ($request->ajax()) {
            return view('candidate.jobs-list-ajax', compact('datas'));
        }

        return view('home', compact('datas'));
    }

    public function logout(Request $req)
    {
        Auth::logout();
        Session::flush();

        return redirect('/');
	}
}

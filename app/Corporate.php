<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Corporate extends Model
{
    protected $table = 'corporate';

    public function rGetJobList() {
        return $this->hasMany(JobList::class, 'id_cp', 'id_cp');
    }
}
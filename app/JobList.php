<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobList extends Model
{
    protected $table = 'joblist';

    public function rGetApplicants() {
        return $this->hasMany(JobApplicant::class, 'id_job', 'id_job');
    }
}

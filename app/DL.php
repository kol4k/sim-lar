<?php

namespace App;

//DataList
class DL
{
    const CategoryJob = [
        'CF' => 'SKKNI Certification Needed',
        'GEN' => 'Common Candidate',
        'DTS' => 'Digitalent Graduate'
    ];

    const TypeCompany = [
        'BUMN', 
        'BUMD', 
        'Swasta', 
        'Startup', 
        'Pemerintah'
    ];

    const TypeJob = [
        'Magang', 'Karyawan Tetap', 'Karyawan Kontrak'
    ];
}

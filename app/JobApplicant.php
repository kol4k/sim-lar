<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplicant extends Model
{
    protected $table = 'apl_job';

    public $timestamps = false;
}

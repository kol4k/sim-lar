function isJson(i) {
    try {
        JSON.parse(i);
    } catch (e) {
        return false;
    }

    return true;
}

function cleanInput(str) {
    var temp = document.createElement('div');
    temp.textContent = str;
    return temp.innerHTML;
}

function notifError(text) {
    Snackbar.show({
        pos: 'top-center',
        backgroundColor: '#f8d7da',
        textColor: '#721c24',
        text: text,
        showAction: true,
        actionText: 'X',
        duration: 0
    });
}

function notifSuccess(text) {
    Snackbar.show({
        pos: 'top-center',
        backgroundColor: '#d4edda',
        textColor: '#155724',
        text: text,
        showAction: true,
        actionText: 'X',
        duration: 3000
    });
}

function setErrorsMsg(obj) {
    var errorsMsg = '';
    if (Array.isArray(obj.errors) || typeof obj.errors === 'object') {
        errorsMsg += '<ul class="errsnack">';
        $.each(obj.errors, function (key, value) {
            if (Array.isArray(value)) {
                for (i = 0; i < value.length; i++) {
                    errorsMsg += '<li >' + value[i] + '</li>';
                }
            } else {
                errorsMsg += '<li>' + value + '</li>';
            }
        });
        errorsMsg += '</ul>';
    } else {
        errorsMsg += '<ul><li>' + obj.errors + '</li></ul>';
    }

    notifError(errorsMsg);
}

function setMsg(obj) {
    if (obj.hasOwnProperty('errors')) {
        setErrorsMsg(obj);
    } else if (obj.hasOwnProperty('success')) {
        notifSuccess(obj.success);
    }
}

function showLoader() {
    $('body').append('<div class="loader"><div class="spinner"></div></div>');
}

function removeLoader() {
    $('.loader').remove();
}

function stopClick(e) {
    if (e.timeStamp - timeClick < 1000) {
        return true;
    } else {
        timeClick = e.timeStamp;
    }
}

timeClick = 0;

$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).on({
        ajaxStart: function () {
            showLoader();
        },
        ajaxStop: function () {
            removeLoader();
        },
        // Dev Mode
        ajaxError: function (e, x, settings, exception) {
            var message,
                statusErrorMap = {
                    '400': 'Server understood the request, but request content was invalid.',
                    '401': 'Sesi anda sudah habis, silakan login kembali',
                    '403': 'Forbidden resource can\'t be accessed.',
                    '500': 'Internal server error.',
                    '503': 'Service unavailable.',
                    '419': 'Sesi anda sudah habis, silakan login kembali',
                };
            if (x.status) {
                message = statusErrorMap[x.status];
                if (!message) {
                    message = " Error HTTP Status " + x.status + "\n.";
                }
            } else if (exception == 'parsererror') {
                message = "Error.\nParsing JSON Request failed.";
            } else if (exception == 'timeout') {
                message = "Request Time out.";
            } else if (exception == 'abort') {
                message = "Request was aborted by the server";
            } else {
                message = "Terjadi Kesalahan Silahkan Mengulangi Proses Kembali\n.";
            }
            notifError(message);
            removeLoader();
            if(x.status === 401 || x.status === 419){
                setTimeout(function () {
                    return window.location.replace("/login");
                }, 2000);
            }
        }
    });
});

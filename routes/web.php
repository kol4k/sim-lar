<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('public.home');

Route::get('jobs', 'CandidateController@jobsLists')->name('public.jobs-list');
Route::get('jobs/{id}/detail', 'CandidateController@jobDetail')->name('public.job-detail');
Route::get('companies', 'CandidateController@findCompany')->name('public.find-company');
Route::get('companies/{id}/detail', 'CandidateController@detailCompany')->name('public.detail-company');

Route::get('/logout', 'HomeController@logout')->name('logout');

Auth::routes();

Route::group(['prefix' => 'candidate'], function () {
    Route::get('start', 'CandidateController@setupProfile')->name('candidate.setup.profile');

    Route::group(['middleware' => 'setup'], function () {
        Route::get('profile', 'CandidateController@profile')->name('candidate.profile');
        Route::post('start', 'CandidateController@setupProfileCreate')->name('candidate.setup.profile.create');
        Route::get('jobs-applicants', 'CandidateController@jobsApplicant')->name('candidate.jobs-applicant');
        Route::get('jobs', 'CandidateController@jobsLists')->name('candidate.jobs-list');
        Route::get('jobs/{id}/detail', 'CandidateController@jobDetail')->name('candidate.job-detail');
        Route::get('jobs/{id}/apply', 'CandidateController@applyJob')->name('candidate.job-apply');
        Route::get('companies', 'CandidateController@findCompany')->name('candidate.find-company');
        Route::get('companies', 'CandidateController@detailCompany')->name('candidate.detail-company');
        Route::get('companies/{id}/detail', 'CandidateController@findCompany')->name('candidate.find-company');
        Route::get('change-password', 'CandidateController@changePassword')->name('candidate.change-password');
    });
});
@extends('layouts.app')

@section('title')
Digital Talent Scholarship | Atur Ulang Password
@endsection

@section('content')
<div id="titlebar" class="gradient">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Reset Password</h2>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-xl-6 col-lg-6 content-right-offset margin-bottom-70">
			<img src="{{ asset('assets/@images/team.png') }}" width="95%">
		</div>

		<div class="col-xl-6 col-lg-6 margin-top-20 margin-bottom-60">
			<div class="boxed-widget summary" style="margin-top:-45px !important">
				<div class="boxed-widget-headline">
					<div class="popup-tabs-container">
						<div class="popup-tab-content" id="tab">
							<div class="welcome-text">
								<h3>Reset Password</h3>
							</div>
							<form method="POST" action="{{ route('password.update') }}">
								@csrf
								<input type="hidden" name="token" value="{{ $token }}">

								@if ($errors->has('email'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
								@endif
								<div class="input-with-icon-left">
									<i class="icon-line-awesome-at"></i>
									<input id="email" type="email" class="input-text with-border{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
								</div>

								@if ($errors->has('password'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('password') }}</strong>
									</span>
								@endif
								<div class="input-with-icon-left">
									<i class="icon-material-outline-https"></i>
									<input id="password" type="password" class="input-text with-border{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Min 8 Karakter (Huruf & Angka)" required autofocus>
								</div>

								<div class="input-with-icon-left">
									<i class="icon-material-outline-https"></i>
									<input id="password" type="password" class="input-text with-border" name="password_confirmation" placeholder="Confirm Password" required autofocus>
								</div>

								<button type="submit" class="button margin-top-25 full-width button-sliding-icon ripple-effect" style="width: 415px;">Reset Password</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="margin-top-70"></div>
@endsection
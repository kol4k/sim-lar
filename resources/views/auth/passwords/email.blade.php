@extends('layouts.app')

@section('js')
	<script>
		function invalidErrorEmail(el) {
			let str = el.value;
			let intervalEt = str.split('@');
			if (str === '') return el.setCustomValidity('Mohon melengkapi format alamat email yang diinputkan');
			else if (str.indexOf('@') !== -1) {
				if (intervalEt.length > 2) {
					return el.setCustomValidity('Alamat email mengandung banyak \'@\'');
				}
				else return el.setCustomValidity('');
			}
			else return el.setCustomValidity('Mohon menambahkan @ di alamat email yang diinputkan');
		}
	</script>
@endsection

@section('title')
Digital Talent Scholarship | Atur Ulang Password
@endsection

@section('content')
<div id="titlebar" class="gradient">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Lupa Password ?</h2>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-xl-6 col-lg-6 content-right-offset margin-bottom-70">
			<img src="{{ asset('assets/@images/team.png') }}" width="95%">
		</div>

		<div class="col-xl-6 col-lg-6 margin-top-20 margin-bottom-60">
			<div class="boxed-widget summary" style="margin-top:-45px !important">
				<div class="boxed-widget-headline">
					<div class="popup-tabs-container">
						<div class="popup-tab-content" id="tab">
							<div class="welcome-text">
								<h3>Reset Password</h3>
							</div>
							<form method="POST" action="{{ route('password.email') }}">
								@csrf
								@if (session('status'))
									<p>{{ session('status') }}</p>
								@endif

								<div class="input-with-icon-left">
									<i class="icon-line-awesome-at"></i>
									<input onvalid="this.setCustomValidity('')" oninvalid="invalidErrorEmail(this)" id="email" type="email" class="input-text with-border{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
									{{--<input data-errormessage-value-missing="Input tidak boleh kosong" id="email" type="email" class="input-text with-border{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>--}}
									@if ($errors->has('email'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
									@endif
								</div>

								<button type="submit" class="button margin-top-25 full-width button-sliding-icon ripple-effect" style="width: 415px;">Kirim Link Reset Password</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="margin-top-70"></div>
@endsection

@extends('layouts.app')

@section('title')
Simonas | Login
@endsection

@section('js')
{!! NoCaptcha::renderJs() !!}
@endsection

@section('content')
<div id="titlebar" class="gradient">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Sign in</h2>
			</div>
		</div>
	</div>
</div>

<div class="container">
	
	
	<div class="row">
		<div class="col-xl-6 col-lg-6 content-right-offset margin-bottom-70">
			<img src="{{ asset('assets/@images/small-ld.png') }}" width="95%">
		</div>

		<div class="col-xl-6 col-lg-6 margin-top-20 margin-bottom-60">
            @if(session('status'))
                <div class="notification success closeable">
                    <p>{{ session('status') }}</p>
                    <a class="close"></a>
                </div>
            @endif
            @if(session('verified'))
                <div class="notification success closeable">
                    <p>Akun anda berhasil diverifikasi, silahkan login kembali</p>
                    <a class="close"></a>
                </div>
            @endif
            @if(session('alreadyVerified'))
                <div class="notification warning closeable">
                    <p>Akun anda sudah diverifikasi sebelumnya, silahkan login kembali</p>
                    <a class="close"></a>
                </div>
            @endif
			<div class="boxed-widget summary">
				<div class="boxed-widget-headline">
					<div class="popup-tabs-container">
						<div class="popup-tab-content" id="tab">
							<div class="welcome-text">
								<h3>Sign in Candidate</h3>
							</div>
							<form method="POST" action="{{ route('login') }}">
								@csrf
								@if ($errors->has('nocertificate'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('nocertificate') }}</strong>
									</span>
								@endif
								<div class="input-with-icon-left">
									<i class="icon-line-awesome-certificate"></i>
									<input id="nocertificate" type="text" class="input-text with-border{{ $errors->has('nocertificate') ? ' is-invalid' : '' }}" name="nocertificate" value="{{ old('nocertificate') }}" placeholder="No. Certificate" required autofocus>
								</div>
								
								@if ($errors->has('password'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('password') }}</strong>
									</span>
								@endif
								<div class="input-with-icon-left">
									<i class="icon-material-outline-https"></i>
									<input id="password" type="password" class="input-text with-border{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
								</div>
								
								<!--input class="" style="display:inline;height:20px;width:20px;" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
								<label class="" style="display:inline;height:auto;width:auto;" for="remember">
									Remember me
								</label-->

								<div>{!! NoCaptcha::display() !!}</div>

								@if ($errors->has('g-recaptcha-response'))
									<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('g-recaptcha-response') }}</strong>
								</span>
								@endif
								
								<button type="submit" class="button margin-top-25 full-width button-sliding-icon ripple-effect" style="width: 415px;">Login</button>
								
								<p class="margin-top-10">
									Lupa Password ? Klik <a href="{{ route('password.request') }}">disini</a><br />
									Belum punya akun ? Klik <a href="{{ route('register') }}">disini</a>
								</p>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="margin-top-70"></div>
@endsection

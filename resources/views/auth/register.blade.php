@extends('layouts.app')

@section('title')
Simonas | Daftar
@endsection

@section('js')
{!! NoCaptcha::renderJs() !!}
@endsection

@section('content')
<div id="titlebar" class="gradient">
	<div class="container">
		<div class="row">
			<div class="col-md-12"><h2>Create an Account</h2></div>
		</div>
	</div>
</div>

<div class="container" style="margin-bottom:80px !important">
	<div class="row">
		<form method="POST" action="{{ route('register') }}">
		@csrf
		<div class="col-xl-12">
			<div class="dashboard-box margin-top-0">
				<div class="headline">
					<h3><i class="icon-material-outline-lock"></i> Account Information</h3>
				</div>
				
				<div class="content with-padding padding-bottom-0">
					<div class="row">
						<div class="col">
							<div class="row">
								<div class="col-xl-4">
									<div class="submit-field">
										<h5>Full name <span style="color:red; margin-left:5px">*</span></h5>
										<input type="text" class="with-border" name="name" value="{{ old('name') }}" placeholder="Full Name" required >
										@if ($errors->has('name'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('name') }}</strong>
											</span>
										@endif
									</div>
								</div>
								
								<div class="col-xl-4">
									<div class="submit-field">
										<h5>No. Identity (KTP) <span style="color:red; margin-left:5px">*</span></h5>
										<input type="text" class="with-border" name="noid" value="{{ old('noid') }}" placeholder="16 Digit" required min="16" maxlength="16">
										@if ($errors->has('noid'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('noid') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="col-xl-4">
									<div class="submit-field">
										<h5>Username <span style="color:red; margin-left:5px">*</span></h5>
										<input type="number" class="with-border" name="nocertificate" value="{{ old('nocertificate') }}" placeholder="No. certificate" required minlength="2">
										@if ($errors->has('nocertificate'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('nocertificate') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="col-xl-4">
									<div class="submit-field">
										<h5>Email <span style="color:red; margin-left:5px">*</span></h5>
										<input type="email" class="with-border" name="email" value="{{ old('email') }}" placeholder="Email harus aktif dan benar" required>
										@if ($errors->has('email'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('email') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="col-xl-4">
									<div class="submit-field">
										<h5>Password <span style="color:red; margin-left:5px">*</span></h5>
										<input type="password" class="with-border" name="password" placeholder="Min 8 Karakter (Huruf & Angka)" required>
										@if ($errors->has('password'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('password') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="col-xl-4">
									<div class="submit-field">
										<h5>Konfirmasi Password <span style="color:red; margin-left:5px">*</span></h5>
										<input type="password" class="with-border" name="password_confirmation" placeholder="Min 8 Karakter (Huruf & Angka)" required>
										@if ($errors->has('password_confirmation'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('password_confirmation') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="col-xl-12">
									<div class="submit-field">
										<h5>Captcha <span style="color:red; margin-left:5px">*</span></h5>

										<div>{!! NoCaptcha::display() !!}</div>

										@if ($errors->has('g-recaptcha-response'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('g-recaptcha-response') }}</strong>
											</span>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-xl-12">
			<button type='submit' class='button ripple-effect big margin-top-30'>Submit</button>
		</div>
		</form>
	</div>
</div>
@endsection

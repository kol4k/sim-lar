@extends('layouts.app')

@section('title')
Digital Talent Scholarship | Verifikasi
@endsection

@section('content')
<div class="section gray padding-top-65 padding-bottom-70 full-width-carousel-fix" style="background: url({{ asset('assets/@images/star-banner.png') }});">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="section-headline centered margin-bottom-15" style="margin-bottom:60px !important">
					<h3>Verifikasi email kamu</h3>
					<br/>
					<p>
						Sebelum lanjut ke fase berikutnya, silakan cek di inbox/spam email kamu untuk melakukan verifikasi akun.
						Jika kamu belum juga menerima email verifikasi, <a href="{{ route('verification.resendConfirm') }}">klik disini untuk mengirimkan ulang email verifikasi</a>.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@extends('layouts.app')

@section('title')
    Digital Talent Scholarship | Resend Email Verifikasi
@endsection

@section('content')
    <div id="titlebar" class="gradient">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Resend Email Verifikasi</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6 content-right-offset margin-bottom-70">
                <img src="{{ asset('assets/@images/team.png') }}" width="95%">
            </div>

            <div class="col-xl-6 col-lg-6 margin-top-20 margin-bottom-60">
                <div class="boxed-widget summary" style="margin-top:-45px !important">
                    <div class="boxed-widget-headline">
                        <div class="popup-tabs-container">
                            <div class="popup-tab-content" id="tab">
                                <div class="welcome-text">
                                    <h3>Kirim Ulang Email Verifikasi</h3>
                                </div>

                                @if (session('resent'))
                                    <p class="notification notice closeable">
                                        Email verifikasi terbaru sudah kami kirimkan ke inbox email kamu.
                                    <a class="close"></a></p>
                                @endif

                                <p>Apakah kamu ingin mengirim ulang email verifikasi?</p>
                                <a href="{{ route('verification.resend') }}" class="button margin-top-25 full-width button-sliding-icon ripple-effect" style="width: 415px;">Kirim ulang email</a>

                                @if(env('OTP_PUBLIC_ACCESS'))
                                    @if($otpShowLink)
                                        <br>
                                        <p class="notification warning closeable">
                                            Kamu belum juga menerima email verifikasi ?
                                            silakan <strong><a href="{{ route('sms.request') }}"> klik disini</a></strong> untuk verifikasi akun via SMS
                                        </p>
                                    @endif
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="margin-top-70"></div>
@endsection

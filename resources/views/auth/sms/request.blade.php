@extends('layouts.app')

@section('title')
    Digital Talent Scholarship | Verifikasi Via SMS
@endsection

@section('content')
    <div id="titlebar" class="gradient">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Verifikasi via SMS</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6 content-right-offset margin-bottom-70">
                <img src="{{ asset('assets/@images/team.png') }}" width="95%">
            </div>

            <div class="col-xl-6 col-lg-6 margin-top-20 margin-bottom-60">
                <div class="boxed-widget summary" style="margin-top:-45px !important">
                    <div class="boxed-widget-headline">
                        <div class="popup-tabs-container">
                            <div class="popup-tab-content" id="tab">
                                <div class="welcome-text">
                                    <h3>Verifikasi via SMS</h3>
                                </div>
                                <form method="POST" action="{{ route('sms.request.check') }}">
                                    @csrf
                                    @if (session('status'))
                                        <p class="notification warning closeable">{{ session('status') }} <a class="close"></a></p>
                                    @endif

                                    <div class="input-with-icon-left">
                                        <i class="icon-line-awesome-at"></i>
                                        <input id="email" type="email" class="input-text with-border{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
                                        @endif
                                    </div>

                                    <button type="submit" class="button margin-top-25 full-width button-sliding-icon ripple-effect" style="width: 415px;">Kirim Kode Verifikasi</button>

                                    <p class="margin-top-10">
                                        Sudah punya kode verifikasi ? Klik <a href="{{ route('sms.verify') }}">disini</a>
                                    </p>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="margin-top-70"></div>
@endsection

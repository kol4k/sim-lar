@extends('layouts.app')

@section('title')
    Digital Talent Scholarship | Verifikasi Kode SMS
@endsection

@section('content')
    <div id="titlebar" class="gradient">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Verifikasi Kode SMS</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">

            <div class="col-xl-6 col-lg-6 content-right-offset margin-bottom-70">
                <img src="{{ asset('assets/@images/small-ld.png') }}" width="95%">
            </div>

            <div class="col-xl-6 col-lg-6 margin-top-20 margin-bottom-60">
                <div class="boxed-widget summary" style="margin-top:-45px !important">
                    <div class="boxed-widget-headline">
                        <div class="popup-tabs-container">
                            <div class="popup-tab-content" id="tab">
                                <div class="welcome-text">
                                    <h3>Verifikasi Kode SMS</h3>
                                </div>
                                <form method="POST" action="{{ route('sms.verify.check') }}">
                                    @csrf
                                    @if (session('status'))
                                        <p class="notification notice closeable">{{ session('status') }} <a class="close"></a></p>
                                    @endif

                                    @if (session('warning'))
                                        <p class="notification warning closeable">{{ session('warning') }} <a class="close"></a></p>
                                    @endif

                                    <div>
                                        <input id="kode_verifikasi" type="text" class="input-text with-border{{ $errors->has('kode_verifikasi') ? ' is-invalid' : '' }}" name="kode_verifikasi" value="{{ old('kode_verifikasi') }}" placeholder="Masukan kode verifikasi disini" minlength="4" maxlength="10" required autofocus>
                                    </div>

                                    <button type="submit" class="button margin-top-25 full-width button-sliding-icon ripple-effect" style="width: 415px;">Verifikasi Kode</button>

                                    <p class="margin-top-10">
                                        Kode verifikasi kamu kadaluarsa ? Klik <a href="{{ route('sms.request') }}">disini</a>
                                    </p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="margin-top-70"></div>
@endsection

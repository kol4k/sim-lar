@extends('layouts.app')

@section('content')
<script>
$(document).on('click', '.loadmore', function() {
	$(this).text('Loading...');
	var ele = $(this).parent('li');
	$.ajax({
		url: "{{ route('candidate.jobs-list') }}",
		type: 'GET',
		data: {
			page: $(this).data('page'),
		},
		success: function(response) {
			if (response) {
				ele.hide();
				$(".joblist").append(response);
			}
		}
	});
});
</script>
<!-- Intro Banner
   ================================================== -->
<!-- add class "disable-gradient" to enable consistent background overlay -->
<div class="intro-banner" data-background-image="/images/home-background.jpg">
   <div class="container">
      <!-- Intro Headline -->
      <div class="row">
         <div class="col-md-12">
            <div class="banner-headline">
               <h3>
                  <strong>Welcome to Simonas</strong>
                  <br>
                  <span>Discover Your Dream Job <strong class="color">or</strong> Hire Talented and Certified Candidate for Your Company, today!</span>
               </h3>
            </div>
         </div>
      </div>
      <!-- Search Bar -->
      <div class="row">
         <div class="col-md-12">
            <form action="{{ route('public.jobs-list') }}" method="get">
               <div class="intro-banner-search-form margin-top-95">
                  <!-- Search Field -->
                  <div class="intro-search-field" style="background-color:#fff !important">
                     <label for ="intro-keywords" class="field-title ripple-effect">What job you want?</label>
                     <input type="text" name="job_name" id="intro-keywords" class="scsg" autocomplete="off" spellcheck="false" placeholder="Job Title or Keywords">
                  </div>
                  <!-- Button -->
                  <div class="intro-search-button">
                     <button class="button ripple-effect">Search</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
      <!-- Stats -->
      <div class="row">
         <div class="col-md-12">
            <ul class="intro-stats margin-top-45 hide-under-992px">
               <li>
                  <strong class='counter'>20</strong>
                  <span>Employer</span>					
               </li>
               <li>
                  <strong class='counter'>24</strong>
                  <span>Jobs Posted</span>					
               </li>
               <li>
                  <strong class='counter'>1776</strong>
                  <span>Certified Candidates</span>
               </li>
            </ul>
         </div>
      </div>
   </div>
</div>
<!-- Content
   ================================================== -->
<!-- Category Boxes -->
<div class="section margin-top-65">
   <div class="container">
      <div class="row">
         <div class="col-xl-12">
            <!-- Category Boxes Container -->
            <div class="categories-container">
               <!-- Category Box -->
               <a href="#" class="category-box">
                  <div class="category-box-icon">
                     <i class="icon-line-awesome-certificate"></i>
                  </div>
                  <div class="category-box-content">
                     <h3>Certified Candidates</h3>
                     <p>Thousands candidates has been certified with SKKNI</p>
                  </div>
               </a>
               <!-- Category Box -->
               <a href="#" class="category-box">
                  <div class="category-box-icon">
                     <i class="icon-line-awesome-briefcase"></i>
                  </div>
                  <div class="category-box-content">
                     <h3>Hot Vacancies</h3>
                     <p>Find your dream job at reputable company & startup</p>
                  </div>
               </a>
               <!-- Category Box -->
               <a href="#" class="category-box">
                  <div class="category-box-icon">
                     <i class="icon-feather-users"></i>
                  </div>
                  <div class="category-box-content">
                     <h3>Find Your Talent</h3>
                     <p>Simple and easy to find the best talent for work</p>
                  </div>
               </a>
               <!-- Category Box -->
               <a href="#" class="category-box">
                  <div class="category-box-icon">
                     <i class="icon-line-awesome-desktop"></i>
                  </div>
                  <div class="category-box-content">
                     <h3>Free Platform</h3>
                     <p>It's a free platform for Candidates and Employers</p>
                  </div>
               </a>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Category Boxes / End -->
<!-- Features Jobs -->
<div class="section gray margin-top-45 padding-top-65">
   <div class="container">
      <div class="row">
         <div class="col-xl-12">
            <!-- Section Headline -->
            <div class="section-headline margin-top-0 margin-bottom-35">
               <h3>Recent Vacancies</h3>
               <a href="{{ route('public.jobs-list') }}" class="headline-link">Browse All Jobs</a>
            </div>
            <!-- Jobs Container -->
            <div class="listings-container compact-list-layout margin-top-35 margin-bottom-75">
               <ul class="joblist" style="list-style-type: none !important; margin:0 !important; padding:0 !important">
				@forelse($datas as $data)
				<!-- Job Listing -->
				<li style='border-bottom:1px solid #f0f0f0 !important'>
					<a href="{{ route('public.job-detail', $data->id_job) }}" class='job-listing with-apply-button'>
						<!-- Job Listing Details -->
						<div class='job-listing-details'>
							<!-- Logo -->
							<div class='job-listing-company-logo'>
								<img src='/company/files/avatar/gambar_7.jpg' alt=''>
							</div>
							<!-- Details -->
							<div class='job-listing-description'>
								<h3 class='job-listing-title'>{{ $data->nm_job .' - '.$data->nm_cp }}</h3>
								<!-- Job Listing Footer -->
								<div class='job-listing-footer'>
									<ul>
										<li><i class='icon-material-outline-location-on'></i> {{ $data->kotakab }}</li>
										<li><i class='icon-material-outline-business-center'></i> {{ $data->type_cp }}</li>
										@if(now() > $data->akhir)
										<li><i class='icon-material-outline-access-time'></i><font style='color:red'><strong>Closed</strong></font></li>
										@else
										<li><i class='icon-material-outline-access-time'></i><font style='color:green'><strong>Open</strong></font></li>
										@endif
									</ul>
								</div>
							</div>
							<!-- Apply Button -->
							<span class='list-apply-button ripple-effect'>View Detail</span>
						</div>
					</a>
				</li>
				@empty
				@endforelse
                  <li class="loadbutton">
                     <button style="width:100% !important; align:center !important;" class="button ripple-effect loadmore" data-page="2">Load More</button>
                  </li>
               </ul>
            </div>
         </div>
         <!-- Jobs Container / End -->
      </div>
   </div>
</div>
</div>
<!-- Featured Jobs / End -->
@endsection

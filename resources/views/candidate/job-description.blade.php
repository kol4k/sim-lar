@extends('layouts.app')

@section('title')
Simonas | Job Detail
@endsection

@section('content')
<!-- Titlebar
================================================== -->
<div class="single-page-header" data-background-image="/images/single-job.jpg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="single-page-header-inner">
                    <div class="left-side">
                        <div class="header-image">
                            <a href="/member/company/detail/16/gojek-indonesia"><img src="/company/files/avatar/gambar_16.jpg" alt=""></a>
                        </div>
                        <div class="header-details">
                            <h3>{{ $datas->nm_job }}</h3>
                            <h5 style="margin-top:-10px">{{ $datas->nm_cp }}</h5>
                            <h5><i class='icon-material-outline-supervisor-account'></i> {{ $datas->rGetApplicants->count() }} Applications</h5>
                        </div>
                    </div>
                    <div class="right-side">
                        <div class="salary-box padding-bottom-0">
                            <div class="salary-type">Deadline</div>
                            <font style='color:green'>{{ $datas->mulai }}</font> - <font style='color:red'>{{ $datas->akhir }}</font>
                            <a href='#small-dialog-1' class='apply-now-button popup-with-zoom-anim' style='margin-top:15px'>Apply Now <i class='icon-material-outline-arrow-right-alt'></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Page Content
================================================== -->
<div class="container">
    <div class="row">

        <!-- Content -->
        <div class="col-xl-8 col-lg-8 content-right-offset margin-bottom-20">

            <div class="single-page-section">
                <h3 class="margin-bottom-20">Job Description & Requirement</h3>
                <div class="page-description">
                {!! $datas->desc_job !!}
                </div>
            </div>

            <div class="single-page-section">
                &nbsp;
                <ul class="list-2">
                </ul>
                <!-- Share Buttons -->
                <div class="share-buttons margin-top-50">
                    <div class="share-buttons-trigger"><i class="icon-feather-share-2"></i></div>
                    <div class="share-buttons-content">
                        <span>Interesting? <strong>Share It!</strong></span>
                        <ul class="share-buttons-icons">
                            <li><a href="#" data-button-color="#3b5998" title="Share on Facebook" data-tippy-placement="top"><i class="icon-brand-facebook-f"></i></a></li>
                            <li><a href="#" data-button-color="#1da1f2" title="Share on Twitter" data-tippy-placement="top"><i class="icon-brand-twitter"></i></a></li>
                            <li><a href="#" data-button-color="#dd4b39" title="Share on Google Plus" data-tippy-placement="top"><i class="icon-brand-google-plus-g"></i></a></li>
                            <li><a href="#" data-button-color="#0077b5" title="Share on LinkedIn" data-tippy-placement="top"><i class="icon-brand-linkedin-in"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <!-- Sidebar -->
        <div class="col-xl-4 col-lg-4">
            <div class="sidebar-container">
                <!-- Sidebar Widget -->
                <div class="sidebar-widget">
                    <div class="job-overview">
                        <div class="job-overview-headline">Job Summary</div>
                        <div class="job-overview-inner">
                            <ul>
                                <li><i class="icon-line-awesome-bullhorn"></i>
                                    <span>Category</span>
                                    <h5>{{ App\DL::CategoryJob[$datas->catjob] }}</h5>
                                </li>
                                <li><i class="icon-material-outline-business"></i>
                                    <span>Employer Type</span>
                                    <h5>{{ $datas->type_cp }}</h5>
                                </li>
                                <li>
                                    <i class="icon-material-outline-location-on"></i>
                                    <span>Location</span>
                                    <h5>{{ $datas->kotakab }}</h5>
                                </li>
                                <li>
                                    <i class="icon-material-outline-business-center"></i>
                                    <span>Job Type</span>
                                    <h5>{{ $datas->type_job }}</h5>
                                </li>
                                <li>
                                    <i class="icon-material-outline-money"></i>
                                    <span>Salary Range (Rp)</span>
                                    <h5>{{ $datas->min_salary }} - {{ $datas->max_salary }}</h5>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>

                <!-- Sidebar Widget -->
                <div class="sidebar-widget">
                    <h3>Share</h3>
                    <!-- Copy URL -->
                    <div class="copy-url">
                        <input id="copy-url" type="text" value="" class="with-border">
                        <button class="copy-url-button ripple-effect" data-clipboard-target="#copy-url" title="Copy to Clipboard" data-tippy-placement="top"><i class="icon-material-outline-file-copy"></i></button>
                    </div>

                </div>

            </div>
        </div>

    </div>

    <div class="single-page-section">
        <h3 class="margin-bottom-25">Similar Jobs</h3>

        <!-- Listings Container -->
        <div class="listings-container grid-layout">

            <!-- Job Listing -->
            @foreach($similiars as $data)
            <a href="{{ route('candidate.job-detail', $data->id_job) }}" class='job-listing'>
                <div class='job-listing-details'>
                    <div class='job-listing-company-logo'>
                        <img src='/company/files/avatar/gambar_30.png' alt=''>
                    </div>
                    <div class='job-listing-description'>
                        <h4 class='job-listing-company'>{{ $data->nm_cp }}</h4>
                        <h3 class='job-listing-title'>{{ $data->nm_job }}</h3>
                    </div>
                </div>
                <div class='job-listing-footer'>
                    <ul>
                        <li><i class='icon-material-outline-location-on'></i> {{ $data->kotakab }}</li>
                        <li><i class='icon-material-outline-business-center'></i> {{ $data->type_job }}</li>
                        <li><i class='icon-material-outline-access-time'></i> <font style='color:green'>{{ $data->mulai }}</font> - <font style='color:red'>{{ $data->akhir }}</font></li>
                    </ul>
                </div>
            </a>
            @endforeach
        </div>
        <!-- Listings Container / End -->

    </div>
</div>

</div>

<!-- Apply for a job popup
================================================== -->
<div id="small-dialog-1" class="zoom-anim-dialog mfp-hide dialog-with-tabs" style="max-width: 540px;">

    <!--Tabs -->
    <div class="sign-in-form">

        <div class="popup-tabs-container">

            <!-- Tab -->
            <div class="popup-tab-content" id="tab">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>Confirmation</h3>
                </div>

                <p>
                    Are you sure to apply 
                    <strong>{{ $datas->nm_job }}</strong> at
                    <strong>{{ $datas->nm_cp }}?</strong>
                </p>
                <!-- Button -->
                <a href="{{ route('candidate.job-apply', $datas->id_job) }}" class="button margin-top-35 full-width button-sliding-icon ripple-effect">Apply Now <i
                            class="icon-material-outline-arrow-right-alt"></i></a>

            </div>

        </div>
    </div>
</div>
<!-- Apply for a job popup / End -->
@endsection

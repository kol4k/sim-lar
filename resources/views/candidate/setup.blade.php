@extends('layouts.app')

@section('title')
Simonas | Completing profile
@endsection

@section('js')
{!! NoCaptcha::renderJs() !!}
@endsection

@section('content')
<div id="titlebar" class="gradient">
	<div class="container">
		<div class="row">
			<div class="col-md-12"><h2>Please Completing your profile</h2></div>
		</div>
	</div>
</div>

<div class="container" style="margin-bottom:80px !important">
	<div class="row">
        <form action="{{ Route('candidate.setup.profile.create') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
            <div class="row">
                <div class='notification warning closeable'><p>Please Complete Your Profile and fill the form correctly before using Simonas (Jika dikemudian hari ditemukan data yang tidak benar, SIMONAS berwenang untuk mengambil tindakan sesuai dengan ketentuan yang berlaku)<br/><strong>Please download and fill this <a href='/template_pernyataan_cv.doc'>Surat Pernyataan Template</a> correctly as required document</strong></h3></p><a class='close'></a></div><br/>					<div class="col-xl-7">
                    <div class="dashboard-box margin-top-15">
                        <div class="headline">
                            <h3><i class="icon-material-outline-account-circle"></i> Personal Information</h3>
                        </div>
                        <div class="content with-padding padding-bottom-0">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="submit-field">
                                        <h5>Full Name</h5>
                                        <input type="text" class="with-border" name="nama" id="nama" value="{{ auth()->user()->name }}" disabled>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <div class="submit-field">
                                        <h5>No. Identity (KTP/SIM/PASSPORT)</h5>
                                        <input type="text" class="with-border" name="noid" id="noid" value="{{ auth()->user()->noid }}" onkeypress="return hanyaAngka(event, false)" maxlength="18" required/>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <div class="submit-field">
                                        <h5>Date of birth</h5>
                                        <input type="date" class="with-border" name="dateofbirth" id="dateofbirth" placeholder="" data-mask="99/99/9999" required>
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="submit-field">
                                        <h5>Genre</h5>
                                        <select class="selectpicker with-border" name="genre" id="genre" required>
                                            <option value="L">Laki-Laki</option>
                                            <option value="P">Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="submit-field">
                                        <h5>Marital</h5>
                                        <select class="selectpicker with-border" name="marital" id="marital" required>
                                            <option value="Lajang">Lajang</option>
                                            <option value="Menikah">Menikah</option>
                                            <option value="Janda">Janda</option>
                                            <option value="Duda">Duda</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <div class="submit-field">
                                        <h5>Address</h5>
                                        <textarea name="address" id="address" maxlength="140" class="with-border" required></textarea>
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="submit-field">
                                        <h5>Province</h5>
                                        <select class="selectpicker with-border" name="province" onchange="select_city(this.value);">
                                            <option value="Aceh">Aceh</option>
                                            <option value="Sumatera Utara">Sumatera Utara</option>
                                            <option value="Sumatera Barat">Sumatera Barat</option>
                                            <option value="Riau">Riau</option>
                                            <option value="Kepulauan Riau">Kepulauan Riau</option>
                                            <option value="Jambi">Jambi</option>
                                            <option value="Bengkulu">Bengkulu</option>
                                            <option value="Sumatera Selatan">Sumatera Selatan</option>
                                            <option value="Kepulauan Bangka Belitung">Kepulauan Bangka Belitung</option>
                                            <option value="Lampung">Lampung</option>
                                            <option value="Banten">Banten</option>
                                            <option value="Jawa Barat">Jawa Barat</option>
                                            <option value="DKI Jakarta">DKI Jakarta</option>
                                            <option value="Jawa Tengah">Jawa Tengah</option>
                                            <option value="Jawa Timur">Jawa Timur</option>
                                            <option value="DI Yogyakarta">DI Yogyakarta</option>
                                            <option value="Bali">Bali</option>
                                            <option value="Nusa Tenggara Barat">Nusa Tenggara Barat</option>
                                            <option value="Nusa Tenggara Timur">Nusa Tenggara Timur</option>
                                            <option value="Kalimantan Barat">Kalimantan Barat</option>
                                            <option value="Kalimantan Selatan">Kalimantan Selatan</option>
                                            <option value="Kalimantan Tengah">Kalimantan Tengah</option>
                                            <option value="Kalimantan Timur">Kalimantan Timur</option>
                                            <option value="Kalimantan Utara">Kalimantan Utara</option>
                                            <option value="Gorontalo">Gorontalo</option>
                                            <option value="Sulawesi Selatan">Sulawesi Selatan</option>
                                            <option value="Sulawesi Tenggara">Sulawesi Tenggara</option>
                                            <option value="Sulawesi Tengah">Sulawesi Tengah</option>
                                            <option value="Sulawesi Utara">Sulawesi Utara</option>
                                            <option value="Sulawesi Barat">Sulawesi Barat</option>
                                            <option value="Maluku">Maluku</option>
                                            <option value="Maluku Utara">Maluku Utara</option>
                                            <option value="Papua">Papua</option>
                                            <option value="Papua Barat">Papua Barat</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="submit-field">
                                        <h5>City</h5>
                                        <select name="city" id="city" class="selectpicker with-border" required>
                                            <option value="Kota Bandarlampung">Kota Bandarlampung</option>
                                        <select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dashboard-box">
                        <!-- Headline -->
                        <div class="headline">
                            <h3><i class="icon-material-outline-library-books"></i> About Me</h3>
                        </div>
                        <div class="content with-padding">
                            <div class="col-xl-12">
                                <div class="submit-field">
                                    <textarea name="biodata" id="biodata" class="with-border" placeholder="Introduce Yourself" required></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dashboard-box">
                        <div class="headline">
                            <h3><i class="icon-material-outline-school"></i> Recent Education Background </h3>
                        </div>
                        <div class="content with-padding">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="submit-field">
                                        <h5>Education Level</h5>
                                        <select name="leveledu" id="leveledu" class="selectpicker with-border" required>
                                            <option value="SMK">SMK</option>
                                            <option value="D3">D3</option>
                                            <option value="D4">D4</option>
                                            <option value="S1">S1</option>
                                            <option value="S2">S2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <div class="submit-field">
                                        <h5>Institution</h5>
                                        <input parsley-trigger="change" class="with-border" type="text" name="univedu" id="univedu" style="text-transform:uppercase !important" required/>
                                    </div>
                                </div>

                                <div class="col-xl-6">
                                    <div class="submit-field">
                                        <h5>Major</h5>
                                        <input parsley-trigger="change" class="with-border" type="text" name="majoredu" id="majoredu" style="text-transform:uppercase !important" required/>
                                    </div>
                                    </div>

                                <div class="col-xl-6">
                                    <div class="submit-field">
                                        <h5>GPA</h5>
                                        <input parsley-trigger="change"  class="with-border" type="text" name="gpaedu" id="gpaedu" style="text-transform:uppercase !important" required/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dashboard-box">
                        <div class="headline">
                            <h3><i class="icon-line-awesome-briefcase"></i> Job Experience</h3>
                        </div>

                        <div class="content">
                            <ul class="dashboard-box-list" id="job-experience-list">
                                <li><h3 class="text-center">Wait a second... Loading</h3></li>
                            </ul>
                            <div class="col-lg-4 margin-left-15 padding-bottom-15">
                                <a href="#small-dialog-1" class="button btn-sm ripple-effect popup-with-zoom-anim">Add</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-5">
                    <div class="dashboard-box margin-top-15">
                        <div class="headline">
                            <h3><i class="icon-brand-telegram-plane"></i> Contact</h3>
                        </div>
                        <div class="content with-padding">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="submit-field">
                                        <h5>Email</h5>
                                        <div class="input-with-icon-left no-border">
                                            <i class="icon-material-outline-account-circle"></i>
                                            <input type="text" class="input-text" placeholder="candidate@work.com" name="email" id="email" value="{{ auth()->user()->email }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <div class="submit-field">
                                        <h5>Phone</h5>
                                        <div class="input-with-icon-left no-border">
                                            <i class="icon-feather-phone"></i>
                                            <input parsley-trigger="change" required onkeypress="return hanyaAngka(event, false)" class="input-text" type="text" name="phone" id="phone" value="{{ auth()->user()->phone }}" placeholder="62xxxxxxxxxxx">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dashboard-box">
                        <div class="headline">
                            <h3><i class="icon-line-awesome-bullhorn"></i> Portfolio & Social Media</h3>
                        </div>
                        <div class="content with-padding">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="submit-field">
                                        <h5>Portfolio Link</h5>
                                        <div class="input-with-icon-left no-border">
                                            <i class="icon-brand-internet-explorer"></i>
                                            <input type="text" name="linkportofolio" id="linkportofolio" class="input-text">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <div class="submit-field">
                                        <h5>Facebook <span>(https://facebook.com/@username)</span></h5>
                                        <div class="input-with-icon-left no-border">
                                            <i class="icon-brand-facebook-f"></i>
                                            <input type="text" name="linkfacebook" id="linkfacebook" class="input-text">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xl-12">
                                    <div class="submit-field">
                                        <h5>Twitter <span>(https://twitter.com/@username)</span></h5>
                                        <div class="input-with-icon-left no-border">
                                            <i class="icon-brand-twitter"></i>
                                            <input type="text" name="linktwitter" id="linktwitter" class="input-text">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xl-12">
                                    <div class="submit-field">
                                        <h5>Instagram <span>(https://instagram.com/@username)</span></h5>
                                        <div class="input-with-icon-left no-border">
                                            <i class="icon-brand-instagram"></i>
                                            <input type="text" name="linkinstagram" id="linkinstagram" class="input-text">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dashboard-box">

                        <!-- Headline -->
                        <div class="headline">
                            <h3><i class="icon-line-awesome-paste"></i> Attachment</h3>
                        </div>

                        <div class="content with-padding">
                            <ul class="dashboard-box-list">
                                <li>
                                    <div class='boxed-list-item'>
                                        <div class="item-content">
                                            <div class="submit-field">
                                                <h5>Photo Formal</h5>
                                                <div class="uploadButton margin-top-30">
                                                    <input class="uploadButton-input" type="file" accept=".jpg,.jpeg" name="gambar" id="upload-gambar" for="name-gambar"/>
                                                    <label class="uploadButton-button ripple-effect" for="upload-gambar">Upload Files</label>
                                                    <span class="uploadButton-file-name" id="name-gambar"></span>
                                                </div>
                                                <h5>* <strong><i>(format jpg/jpeg) - Ukuran file maksimal 5 MB</i></strong></h5>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class='boxed-list-item'>
                                        <div class="item-content">
                                            <div class="submit-field">
                                                <h5>Curicullum Vitae / Resume</h5>
                                                <div class="uploadButton margin-top-30">
                                                    <input class="uploadButton-input" type="file" accept="application/pdf" name="cv" id="upload-cv" for="name-cv"/>
                                                    <label class="uploadButton-button ripple-effect" for="upload-cv">Upload Files</label>
                                                    <span class="uploadButton-file-name" id="name-cv"></span>
                                                </div>
                                                <h5>* <strong><i>(format pdf) - Ukuran file maksimal 1 MB</i></strong></h5>
                                                <h5>* Wajib melampirkan surat pernyataan sesuai template yang telah disediakan.</h5>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class='boxed-list-item'>
                                        <div class="item-content">
                                            <div class="submit-field">
                                                <h5>Ijazah dan Transkrip Nilai</h5>
                                                <div class="uploadButton margin-top-30">
                                                    <input class="uploadButton-input" type="file" accept="application/pdf" name="ijazah" id="upload-ijazah" for="name-ijazah"/>
                                                    <label class="uploadButton-button ripple-effect" for="upload-ijazah">Upload Files</label>
                                                    <span class="uploadButton-file-name" id="name-ijazah"></span>
                                                </div>
                                                <h5>* <strong><i>(format pdf) - Ukuran file maksimal 1 MB</i></strong></h5>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12">
                    <button type='submit' class='button ripple-effect big margin-top-30'>Save Changes</button>
                </div>
            </div>
        </form>
	</div>
</div>
@endsection
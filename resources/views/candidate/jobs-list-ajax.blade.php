@forelse($datas as $data)
<!-- Job Listing -->
<li style='border-bottom:1px solid #f0f0f0 !important'>
    <a href="{{ route('public.job-detail', $data->id_job) }}" class='job-listing with-apply-button'>
        <!-- Job Listing Details -->
        <div class='job-listing-details'>
            <!-- Logo -->
            <div class='job-listing-company-logo'>
                <img src='/company/files/avatar/gambar_7.jpg' alt=''>
            </div>
            <!-- Details -->
            <div class='job-listing-description'>
                <h3 class='job-listing-title'>{{ $data->nm_job .' - '.$data->nm_cp }}</h3>
                <!-- Job Listing Footer -->
                <div class='job-listing-footer'>
                    <ul>
                        <li><i class='icon-material-outline-location-on'></i> {{ $data->kotakab }}</li>
                        <li><i class='icon-material-outline-business-center'></i> {{ $data->type_cp }}</li>
                        @if(now() > $data->akhir)
                        <li><i class='icon-material-outline-access-time'></i><font style='color:red'><strong>Closed</strong></font></li>
                        @else
                        <li><i class='icon-material-outline-access-time'></i><font style='color:green'><strong>Open</strong></font></li>
                        @endif
                    </ul>
                </div>
            </div>
            <!-- Apply Button -->
            <span class='list-apply-button ripple-effect'>View Detail</span>
        </div>
    </a>
</li>
@empty
@endforelse
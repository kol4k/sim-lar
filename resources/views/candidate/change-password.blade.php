@extends('layouts.peserta')

@section('title')
Simonas | Change Password
@endsection

@section('content')
<script>
@if(Request::ajax())
document.title = 'Simonas | Change Password';
pushGaPv('/candidate/change-password');
@endif
</script>
<div class="dashboard-headline">
    <h3>Change Password</h3>
</div>

<!-- Row -->
<div class="row">

<!-- Dashboard Box -->
    <div class="col-xl-12">
        <div id="test1" class="dashboard-box">

            <!-- Headline -->
            <div class="headline">
                <h3><i class="icon-material-outline-lock"></i> Password & Security</h3>
            </div>

            <div class="content with-padding">
                <form  action='/member/reg_setting_tr' method='post' enctype='multipart/form-data'>
                    <div class="row">

                        <div class='col-xl-4'>
                            <div class='submit-field'>
                                <h5>Current Password</h5>
                                <input type='password' name='cpasswd' class='with-border' required>
                                <input type='hidden' name='id_tr' parsley-trigger='change'  required class='form-control' id='id_tr' value='46060'>
                            </div>
                        </div>

                        <div class='col-xl-4'>
                            <div class='submit-field'>
                                <h5>New Password</h5>
                                <input type='password' name='newpasswd' class='with-border' required>
                            </div>
                        </div>

                        <div class='col-xl-4'>
                            <div class='submit-field'>
                                <h5>Repeat New Password</h5>
                                <input type='password' name='rppasswd' class='with-border' required>
                            </div>
                        </div>									
                        <!-- Button -->
                        <div class="col-xl-12">
                            <button type='update' name='update' class='button ripple-effect big margin-top-30'>
                                Save Changes
                            </button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<!-- Row / End -->

@endsection

@extends('layouts.peserta')

@section('title')
Simonas | Jobs Applicant
@endsection

@section('content')
<script>
@if(Request::ajax())
document.title = 'Simonas | Jobs Applicant';
pushGaPv('/candidate/jobs-applicants');
@endif
</script>
<div class="dashboard-headline">
    <h3>Jobs Applicant</h3>
</div>
<!-- Row -->
<div class="row">
   <!-- Dashboard Box -->
   <div class="col-xl-12">
      <div class="dashboard-box margin-top-0">
         <!-- Headline -->
         <div class="headline">
            <h3><i class="icon-material-outline-business-center"></i> My Job Listings</h3>
         </div>
         <div class="content">
            <ul class="dashboard-box-list">
            @forelse($datas as $data)
               <li>
                  <!-- Job Listing -->
                  <div class='job-listing'>
                     <!-- Job Listing Details -->
                     <div class='job-listing-details'>
                        <a href="{{ route('candidate.job-detail', $data->id_job) }}" class='job-listing-company-logo'>
                           <img src='/company/files/avatar/gambar_30.png' alt=''>
                           <!-- Details -->
                           <div class='job-listing-description' >
                              <h3 class='job-listing-title' style='margin-left:-25px !important'>
                        <a href="{{ route('candidate.job-detail', $data->id_job) }}">{{ $data->nm_job }} - {{ $data->nm_cp }}</a></h3>
                        <!-- Job Listing Footer -->
                        <div class='job-listing-footer'>
                        <ul>
                        <li><span class='dashboard-status-button' style='background-color:#fbfadd;color:#8f872e'>Application Submited</span></li>
                        <li><i class='icon-material-outline-date-range'></i> Applied on {{ $data->tglapl }}</li>
                        <li><i class='icon-material-outline-date-range'></i> Closed on <font style='color:red'>18/01/2020</font></li>
                        </ul>
                        </div>
                        </div>
                     </div>
                  </div>
               </li>
            @empty
            @endforelse
            </ul>
         </div>
      </div>
   </div>
</div>
<!-- Row / End -->
@endsection

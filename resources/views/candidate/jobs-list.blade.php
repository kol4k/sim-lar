@php
$params = [];
if (request()->province) $params = ['province' => request()->province];
if (request()->company_type) $params = ['company_type' => request()->company_type];
if (request()->job_type) $params = ['job_type' => request()->job_type];
if (request()->job_name) $params = ['job_name' => request()->job_name];
@endphp
@extends('layouts.app')

@section('title')
Simonas | Job Lists
@endsection

@section('content')
<script>
$(document).on('click', '.loadmore', function() {
	$(this).text('Loading...');
	var ele = $(this).parent('li');
	$.ajax({
		url: "{{ route('candidate.jobs-list', $params) }}",
		type: 'GET',
		data: {
			page: $(this).data('page'),
		},
		success: function(response) {
			if (response) {
				ele.hide();
				$(".joblist").append(response);
			}
		}
	});
});

@if(Request::ajax())
document.title = 'Simonas | Job Lists';
pushGaPv('/candidate/joblists');
@endif
</script>
<div class="full-page-container">
	<div class="full-page-sidebar">
		<form method="GET">
			<div class="full-page-sidebar-inner" data-simplebar>
				<div class="sidebar-container">
					<!-- Location -->
					<div class="sidebar-widget">
						<h3>Location</h3>
						<select name="province" id="province" class="selectpicker with-border">
						<option value="">Choose Location</option>
						@foreach($provinces as $province)
							<option value="{{ $province->nm_prov }}" {{ $province->nm_prov == request()->province ? 'selected' : '' }}>{{ $province->nm_prov }}</option>
						@endforeach
						</select>

						<h3 style="margin-top:50px">Company Type</h3>
						<select name="company_type" id="company_type" class="selectpicker with-border">
							<option value="">Choose Company Type</option>
							@foreach(App\DL::TypeCompany as $type)
							<option value="{{ $type }}" {{ $type == request()->company_type ? 'selected' : '' }}>{{ $type }}</option>
							@endforeach
						</select>

						<h3 style="margin-top:50px">Job Type</h3>
						<select name="job_type" id="job_type" class="selectpicker with-border">
							<option value="">Choose Job Type</option>
							@foreach(App\DL::TypeJob as $type)
							<option value="{{ $type }}" {{ $type == request()->job_type ? 'selected' : '' }}>{{ $type }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<!-- Sidebar Container / End -->

				<!-- Search Button -->
				<div class="sidebar-search-button-container">
					<button type="submit" class="button ripple-effect">Search</button>
				</div>
				<!-- Search Button / End-->

			</div>
		</form>
	</div>
<!-- Full Page Sidebar / End -->


<!-- Full Page Content -->
<div class="full-page-content-container" data-simplebar>
	<div class="full-page-content-inner">

		<h3 class="page-title">Recent Vacancies</h3>

		<div class="listings-container compact-list-layout margin-top-35 margin-bottom-75">

			<ul class="joblist" style="list-style-type: none !important; margin:0 !important; padding:0 !important">
				@forelse($datas as $data)
				<!-- Job Listing -->
				<li style='border-bottom:1px solid #f0f0f0 !important'>
					<a href="{{ route('public.job-detail', $data->id_job) }}" class='job-listing with-apply-button'>
						<!-- Job Listing Details -->
						<div class='job-listing-details'>
							<!-- Logo -->
							<div class='job-listing-company-logo'>
								<img src='/company/files/avatar/gambar_7.jpg' alt=''>
							</div>
							<!-- Details -->
							<div class='job-listing-description'>
								<h3 class='job-listing-title'>{{ $data->nm_job .' - '.$data->nm_cp }}</h3>
								<!-- Job Listing Footer -->
								<div class='job-listing-footer'>
									<ul>
										<li><i class='icon-material-outline-location-on'></i> {{ $data->kotakab }}</li>
										<li><i class='icon-material-outline-business-center'></i> {{ $data->type_cp }}</li>
										@if(now() > $data->akhir)
										<li><i class='icon-material-outline-access-time'></i><font style='color:red'><strong>Closed</strong></font></li>
										@else
										<li><i class='icon-material-outline-access-time'></i><font style='color:green'><strong>Open</strong></font></li>
										@endif
									</ul>
								</div>
							</div>
							<!-- Apply Button -->
							<span class='list-apply-button ripple-effect'>View Detail</span>
						</div>
					</a>
				</li>
				@empty
				@endforelse
				<li class="loadbutton">
					<button style="width:100% !important; align:center !important;" class="button ripple-effect loadmore" data-page="2">Load More</button>
				</li>
			</ul>




		<!-- Jobs Container / End -->

	</div>


@endsection

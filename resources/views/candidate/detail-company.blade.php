@extends('layouts.app')

@section('title')
Simonas | Company Detail
@endsection

@section('content')
<!-- Titlebar
   ================================================== -->
   <div class="single-page-header" data-background-image="/images/single-company.jpg">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="single-page-header-inner">
               <div class="left-side">
                  <div class="header-image"><img src="{{ asset('assets/company/files/avatar/'.$datas->photo_cp) }}" alt=""></div>
                  <div class="header-details">
                     <h3>{{ $datas->nm_cp }}</h3>
                     <ul>
                        <li><i class="icon-material-outline-business"></i>
						{{ $datas->type_cp }}
                        </li>
                        <li><i class="icon-feather-globe"></i>
						{{ $datas->web_cp }}
                        </li>
                        <li><i class="icon-material-outline-email"></i>
						{{ $datas->email_cp }}
                        </li>
                        <li><i class="icon-feather-phone-call"></i> {{ $datas->phone_cp }}</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Page Content
   ================================================== -->
<div class="container">
   <div class="row">
      <!-- Content -->
      <div class="col-xl-8 col-lg-8 content-right-offset">
         <div class="single-page-section">
            <h3 class="margin-bottom-25">About Company</h3>
            {!! $datas->bio_cp !!}			
         </div>
         <!-- Boxed List -->
         <div class="boxed-list margin-bottom-60">
            <div class="boxed-list-headline">
               <h3><i class="icon-material-outline-business-center"></i> Open Positions</h3>
            </div>
            <div class="listings-container compact-list-layout">
			@foreach($datas->rGetJobList->sortByDesc('mulai') as $job)
               <!-- Job Listing -->
               <a href="{{ route('public.job-detail', $job->id_job) }}" class='job-listing'>
                  <!-- Job Listing Details -->
                  <div class='job-listing-details'>
                     <!-- Details -->
                     <div class='job-listing-description'>
                        <h3 class='job-listing-title'>{{ $job->nm_job }}</h3>
                        <!-- Job Listing Footer -->
                        <div class='job-listing-footer'>
                           <ul>
                              <li><i class='icon-material-outline-location-on'></i> {{ $job->kotakab }}</li>
                              <li><i class='icon-material-outline-business-center'></i> {{ $job->type_job }}</li>
                              <li><i class='icon-material-outline-access-time'></i> <font style='color:green'>{{ $job->mulai }}</font> - <font style='color:red'>{{ $job->akhir }}</font></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </a>
			   @endforeach
            </div>
         </div>
         <!-- Boxed List / End -->
      </div>
      <!-- Sidebar -->
      <div class="col-xl-4 col-lg-4">
         <div class="sidebar-container">
            <!-- Location -->
            <div class="sidebar-widget">
               <h3><i class="icon-material-outline-business"></i> Address</h3>
               <div id="single-job-map-container">
                  {{ $datas->add_cp }}
               </div>
            </div>
            <!-- Widget -->
            <div class="sidebar-widget">
               <h3><i class="icon-brand-telegram-plane"></i> Social Profiles</h3>
               <div class="freelancer-socials margin-top-25">
                  <ul>
                     <li><a href='{{ $datas->fb_cp }}' target='_blank' title='Facebook' data-tippy-placement='top'><i class='icon-brand-facebook'></i></a></li>
                     <li><a href='{{ $datas->ig_cp }}' target='_blank' title='Instagram' data-tippy-placement='top'><i class='icon-brand-instagram'></i></a></li>
                     <li><a href='{{ $datas->tw_cp }}' target='_blank' title='Twitter' data-tippy-placement='top'><i class='icon-brand-twitter'></i></a></li>
                  </ul>
               </div>
            </div>
            <!-- Sidebar Widget -->
            <div class="sidebar-widget">
               <!-- Copy URL -->
               <div class="copy-url">
                  <input id="copy-url" type="text" value="" class="with-border">
                  <button class="copy-url-button ripple-effect" data-clipboard-target="#copy-url" title="Copy to Clipboard" data-tippy-placement="top"><i class="icon-material-outline-file-copy"></i></button>
               </div>
               <!-- Share Buttons -->
               <div class="share-buttons margin-top-25">
                  <div class="share-buttons-trigger"><i class="icon-feather-share-2"></i></div>
                  <div class="share-buttons-content">
                     <span>Interesting? <strong>Share It!</strong></span>
                     <ul class="share-buttons-icons">
                        <li><a href="#" data-button-color="#3b5998" title="Share on Facebook" data-tippy-placement="top"><i class="icon-brand-facebook-f"></i></a></li>
                        <li><a href="#" data-button-color="#1da1f2" title="Share on Twitter" data-tippy-placement="top"><i class="icon-brand-twitter"></i></a></li>
                        <li><a href="#" data-button-color="#dd4b39" title="Share on Google Plus" data-tippy-placement="top"><i class="icon-brand-google-plus-g"></i></a></li>
                        <li><a href="#" data-button-color="#0077b5" title="Share on LinkedIn" data-tippy-placement="top"><i class="icon-brand-linkedin-in"></i></a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

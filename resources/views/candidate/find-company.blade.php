@extends('layouts.app')

@section('title')
Simonas | Company List
@endsection

@section('content')
<!-- Titlebar
   ================================================== -->
   <div id="titlebar" class="gradient">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <h2>Browse Companies</h2>
         </div>
      </div>
   </div>
</div>
<!-- Page Content
   ================================================== -->
<div class="container">
   <div class="row">
      <div class="col-xl-12">
         <div class="companies-list">
            @foreach($datas as $data)
            <a href="{{ route('public.detail-company', $data->id_cp) }}" class='company'>
               <div class='company-inner-alignment'>
                  <span class='company-logo'><img src="{{ asset('assets/company/files/avatar/'.$data->photo_cp) }}" alt=''></span>
                  <h4>{{ $data->nm_cp }}</h4>
               </div>
            </a>
            @endforeach
         </div>
      </div>
   </div>
</div>
@endsection

@guest
<div class="header-notifications-trigger">
	<a href="javascript:void(0);"><i class="icon-material-outline-account-circle"></i> Login</a>
</div>

<div class="header-notifications-dropdown">
	<div class="header-notifications-headline">
		<h4>Akun</h4>
		<button class="mark-as-read ripple-effect-dark" data-tippy-placement="left">
			<i class="icon-material-outline-lock"></i>
		</button>
	</div>
	
	<div class="header-notifications-content">
		<div class="header-notifications-scroll" data-simplebar="init" style="height: auto;">
			<div class="simplebar-track vertical" style="visibility: visible; display: none;">
				<div class="simplebar-scrollbar" style="visibility: visible; top: 0px; height: 25px;"></div>
			</div>
			<div class="simplebar-track horizontal" style="visibility: visible; display: none;">
				<div class="simplebar-scrollbar" style="visibility: visible; left: 0px; width: 25px;"></div>
			</div>
			<div class="simplebar-scroll-content" style="padding-right: 17px; margin-bottom: -34px;">
				<div class="simplebar-content" style="padding-bottom: 17px; margin-right: -17px;">
					<ul>
						<li class="notifications-not-read">
							<a href="{{ route('login') }}">
								<span class="notification-icon"><i class="icon-feather-users"></i></span>
								<span class="notification-text">
									Sign in as <span class="color">Candidate</span>
								</span>
							</a>
						</li>
						<li>
							<a href="{{ route('register') }}">
								<span class="notification-icon"><i class="icon-material-outline-assignment"></i></span>
								<span class="notification-text">
									Create an <span class="color">Account</span>
								</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
@else
<div class="header-notifications-trigger">
	<a href="javascript:void(0);">
		<div class="user-avatar status-online">
			@php
			$profile_pic = (!empty(session('ud_foto'))) ?
				route('aplikasi.unduh.file').'?tipeFile=peserta-foto-small&namaFile='. session('ud_foto') :
				'assets/@images/user-placeholder.png';
			@endphp
			<img src="{{ asset($profile_pic) }}" width="60" height="45">
		</div>
	</a>
</div>

<div class="header-notifications-dropdown">
	<div class="user-status">
		<div class="user-details">
			<div class="user-avatar status-online">
				<img src="{{ asset($profile_pic) }}" alt="Foto Proflie" width="60" height="45">
			</div>
			<div class="user-name">
				<h5 style="color:#333;">{{ Auth::user()->name }}</h5>
			</div>
		</div>
	</div>

	<ul class="user-menu-small-nav">
		<li><a href="{{ route('candidate.profile') }}"><img src="{{ asset('assets/@images/icons/employee (1).png') }}" style="width:24px !important"> Profil</a></li>
		<li><a href="{{ route('candidate.change-password') }}"><img src="{{ asset('assets/@images/icons/settings (1).png') }}" style="width:24px !important"> Change Password</a></li>
		<li><a href="{{ route('logout') }}"><img src="{{ asset('assets/@images/icons/off.png') }}" style="width:24px !important"> Logout</a></li>
	</ul>
</div>
@endguest

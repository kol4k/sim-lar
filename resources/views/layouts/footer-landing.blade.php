<div id="footer">
    <div class="footer-top-section">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="footer-rows-container">
                        <div class="footer-rows-left">
                            <div class="footer-row">
                                <div class="footer-row-inner footer-logo">
                                    <img src="{{ asset('assets/@images/logo-2.png') }}" alt="Logo DTS">
                                </div>
                            </div>
                        </div>

                        <div class="footer-rows-right">
                            <div class="footer-row">
                                <div class="footer-row-inner">
                                    <ul class="footer-social-links">
                                        <li>
                                            <a href="https://facebook.com/Digital-Talent-Kominfo-Scholarship-294339374614608/" title="Facebook" data-tippy-theme="light" target="_blank">
                                                <i class="icon-brand-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://twitter.com/DTS_kominfo" title="Twitter" data-tippy-theme="light" target="_blank">
                                                <i class="icon-brand-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://instagram.com/digitalent.kominfo" title="Instagram" target="_blank" data-tippy-theme="light">
                                                <i class="icon-brand-instagram"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://youtube.com/channel/UCeaEf_enHQ-StkN6I3ZOtbg" title="Youtube" target="_blank" data-tippy-theme="light">
                                                <i class="icon-brand-youtube"></i>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-middle-section">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="footer-links">
                        <h3>Alamat</h3>
                        <p>
                            Kementerian Komunikasi dan Informatika RI<br/>
                            Jl. Medan Merdeka Barat No. 9 <br/>
                            Jakarta Pusat, 10110 <br/><br/>
                        </p>
                    </div>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="footer-links">
                        <h3>Link Eksternal</h3>
                        <ul>
                            <li><a href="https://kominfo.go.id/" target="_blank"><span>Kementerian Kominfo</span></a></li>
                            <li><a href="https://balitbangsdm.kominfo.go.id/" target="_blank"><span>Badan Litbang SDM Kominfo</span></a></li>
                            <li><a href="https://proserti.kominfo.go.id/" target="_blank"><span>Pusbang Proserti</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom-section">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    @include('layouts.partials.copyright')
                </div>
            </div>
        </div>
    </div>
</div>

@include('layouts.partials.analytics')

</body>
</html>
@php
// Profiling
$salt = 'dTs2019#@!';
$token = sha1(sha1(trim(auth()->user()->id) . trim(auth()->user()->email)) . $salt);

$profiling = env('URL_PROFILING', '').'/token/'.$token;
@endphp

@if(!Request::ajax())
 
@include('layouts.header')

<script>
$(function() {
	$(document).on('click', 'a.menu-peserta', function(e) {
		e.preventDefault();
		if(stopClick(e)) { return false; }
		
		var $this = $(this);
		$.ajax({
			url: $this.attr('href'),
			success: function (res) {
				$('.content-peserta').html(res);
				$('.menu-peserta li').removeClass('active');
				$this.closest('li').addClass('active');
			}
		});
		
		return false;
	});
	
	$(document).on('click', '.btn-profil-link', function(e) {
		e.preventDefault();
		if(stopClick(e)) { return false; }
		
		var $this = $(this);
		$.ajax({
			type: 'GET',
			url: $this.data('href'),
			success: function (res) {
				if (res.hasOwnProperty('errors')) {
					setMsg(res);
				} else {
					$('.content-peserta').html(res);
					$('.selectpicker').selectpicker();
				}
			}
		});
		
		return false;
	});
	
	$(document).on('click', '.a-profil-link', function(e) {
		e.preventDefault();
		if(stopClick(e)) { return false; }
		
		var $this = $(this);
		$.ajax({
			type: 'GET',
			url: $this.attr('href'),
			success: function (res) {
				if (res.hasOwnProperty('errors')) {
					setMsg(res);
				} else {
					$('.content-peserta').html(res);
					$('.selectpicker').selectpicker();
				}
			}
		});
		
		return false;
	});
});
</script>

<div class="dashboard-container">
	<div class="dashboard-sidebar">
		<div class="dashboard-sidebar-inner" data-simplebar>
			<div class="dashboard-nav-container">
				<a href="javascript:void(0);" class="dashboard-responsive-nav-trigger">
					<span class="hamburger hamburger--collapse" >
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</span>
					<span class="trigger-title">Dashboard Navigation</span>
				</a>
				<div class="dashboard-nav">
					<div class="dashboard-nav-inner">
						<ul data-submenu-title="Menu" class="menu-peserta">
							<li class="active"><a href="{{ route('candidate.profile') }}" class="menu-peserta"><img src="{{ asset('assets/@images/icons/employee (1).png') }}" data-sga="pendaftaran" style="width:24px !important"> Profile</a></li>
							<li><a href="{{ route('candidate.jobs-applicant') }}" class="menu-peserta"><img src="{{ asset('assets/@images/icons/briefcase (3).png') }}" data-sga="jobs-applicant" style="width:24px !important"> Jobs Applicantions</a></li>
							<li><a href="{{ route('candidate.change-password') }}" class="menu-peserta"><img src="{{ asset('assets/@images/icons/settings (1).png') }}" data-sga="setting" style="width:24px !important"> Change Password</a></li>
							<li><a href="{{ route('logout') }}"><img src="{{ asset('assets/@images/icons/off.png') }}" style="margin-top:-7px !important; width:24px !important"> Logout</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="dashboard-content-container" data-simplebar>
		
				<div class="dashboard-content-inner">
					<div class="content-peserta">
						@yield('content')
					</div>
					
					<div class="margin-top-15"></div>

					<div class="dashboard-footer-spacer" style="padding-top: 123px;"></div>
					<div class="small-footer margin-top-15">
						<div class="small-footer-copyrights">
							@include('layouts.partials.copyright')
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
	</div>
</div>
</div>
@include('layouts.partials.analytics')
</body>
</html>
@else

@yield('content')

@endif
<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,800&subset=latin-ext" rel="stylesheet">

<link href="{{ mix('assets/css/App.css') }}" rel="stylesheet">
<link href="{{ mix('assets/css/upload.css') }}" rel="stylesheet">


@yield('css')
<!--[if lt IE 9]>
<script src="{{ asset('assets/js/html5shiv.min.js') }}"></script>
<script src="{{ asset('assets/js/respond.min.js') }}"></script>
<![endif]-->

<script src="{{ mix('assets/js/App.js') }}"></script>

@yield('js')
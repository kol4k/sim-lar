<!DOCTYPE html>
<html lang="en">
<head>
<meta name="robots" content="NOINDEX, NOFOLLOW">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>Verifikator DTS 2019</title>
<link rel="shortcut icon" href="favicon.ico">

<link rel="stylesheet" href="{{ asset('assets/verif/fonts/open-sans/style.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/verif/fonts/universe-admin/style.css') }}">
<link rel="stylesheet" href="{{ asset('assets/verif/fonts/mdi/css/materialdesignicons.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/verif/fonts/iconfont/style.css') }}">
<link rel="stylesheet" href="{{ asset('assets/verif/vendor/flatpickr/flatpickr.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/verif/vendor/simplebar/simplebar.css') }}">
<link rel="stylesheet" href="{{ asset('assets/verif/vendor/tagify/tagify.css') }}">
<link rel="stylesheet" href="{{ asset('assets/verif/vendor/tippyjs/tippy.css') }}">
<link rel="stylesheet" href="{{ asset('assets/verif/vendor/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/verif/vendor/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/verif/css/style.min.css') }}">

<script src="{{ asset('assets/verif/js/ie.assign.fix.min.js') }}"></script>
<script src="{{ asset('assets/verif/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/verif/vendor/popper/popper.min.js') }}"></script>
<script src="{{ asset('assets/verif/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/verif/vendor/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('assets/verif/vendor/simplebar/simplebar.js') }}"></script>
<script src="{{ asset('assets/verif/vendor/text-avatar/jquery.textavatar.js') }}"></script>
<script src="{{ asset('assets/verif/vendor/tippyjs/tippy.all.min.js') }}"></script>
<script src="{{ asset('assets/verif/vendor/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ asset('assets/verif/vendor/wnumb/wNumb.js') }}"></script>
<script src="{{ asset('assets/verif/js/main.js') }}"></script>
<script src="{{ asset('assets/verif/js/preview/settings-panel.min.js') }}"></script>
<script src="{{ asset('assets/verif/js/preview/slide-nav.min.js') }}"></script>

<link href="{{ asset('assets/verif/css/snackbar.min.css') }}" rel="stylesheet">
<script src="{{ asset('assets/verif/js/snackbar.min.js') }}"></script>

<script>
function isJson(i) {
	try {
		JSON.parse(i);
	} catch (e) {
		return false;
	}
	
	return true;
}

function cleanInput(str) {
	var temp = document.createElement('div');
	temp.textContent = str;
	return temp.innerHTML;
}

function notifError(text) {
	Snackbar.show({
		pos: 'top-center',
		backgroundColor: '#f8d7da',
		textColor: '#721c24',		
		text: text,
		showAction: true,
		actionText: 'X',
		duration: 0
	});
}

function notifSuccess(text) {
	Snackbar.show({
		pos: 'top-center',
		backgroundColor: '#d4edda',
		textColor: '#155724',			
		text: text,
		showAction: true,
		actionText: 'X',
		duration: 3000
	});
}

function setErrorsMsg(obj) {
	var errorsMsg = '';
	if(Array.isArray(obj.errors) || typeof obj.errors === 'object') {
		errorsMsg += '<ul class="errsnack">';
		$.each(obj.errors, function(key, value) {
			if (Array.isArray(value)) {
				for (i = 0; i < value.length; i++) { 
					errorsMsg += '<li >' + value[i] + '</li>';
				}
			} else {
				errorsMsg += '<li>' + value + '</li>';
			}
		});
		errorsMsg += '</ul>';
	} else {
		errorsMsg += '<ul><li>' + obj.errors + '</li></ul>';
	}
	
	notifError(errorsMsg);
}

function setMsg(obj) {
	if (obj.hasOwnProperty('errors')) {
		setErrorsMsg(obj);
	} else if (obj.hasOwnProperty('success')) {
		notifSuccess(obj.success);
	}
}

function showLoader() {
	$('body').append('<div class="loader"><div class="spinner"></div></div>');
}

function removeLoader() {
	$('.loader').remove();
}

function stopClick(e) {
	if(e.timeStamp - timeClick < 1000) {
		return true;
	} else {
		timeClick = e.timeStamp;
	}
}

timeClick = 0;

function statusSeleksi(s) {
	var list = [
		'<span class="badge badge-secondary">MENUNGGU</span>',
		'<span class="badge badge-danger">TIDAK LULUS SELEKSI</span>',
		'<span class="badge badge-info">TES BIDANG</span>',
		'<span class="badge badge-danger">TIDAK LULUS TES BIDANG</span>',
		'<span class="badge badge-success">LULUS TES BIDANG</span>',
		'<span class="badge badge-danger">DITOLAK</span>',
		'<span class="badge badge-buttercup">CADANGAN</span>',
		'<span class="badge badge-success">DITERIMA</span>'
	];
	return list[s];
}

function statusBerkas(s) {
	var list = [
		'<span class="badge badge-secondary">UNVERIFIED</span>',
		'<span class="badge badge-danger">INCOMPLETE</span>',
		'<span class="badge badge-success">VERIFIED</span>'
	];
	return list[s];
}

$(function() {
	$.ajaxSetup({
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	$(document).on({
		ajaxStart : function() {
			showLoader();
		},
		ajaxStop : function() {
			removeLoader();
		},
		// Dev Mode
		ajaxError : function(e, x, settings, exception) {
			var message,
				statusErrorMap = {
				'400' : 'Server understood the request, but request content was invalid.',
				'401' : 'Unauthorized access. Session is invalid.',
				'403' : 'Forbidden resource can\'t be accessed.',
				'500' : 'Internal server error.',
				'503' : 'Service unavailable.'
			};
			if (x.status) {
				message = statusErrorMap[x.status];
				if (!message) {
					message = " Error HTTP Status " + x.status + "\n.";
				}
			} else if (exception == 'parsererror') {
				message = "Error.\nParsing JSON Request failed.";
			} else if (exception == 'timeout') {
				message = "Request Time out.";
			} else if (exception == 'abort') {
				message = "Request was aborted by the server";
			} else {
				message = "Terjadi Kesalahan Silahkan Mengulangi Proses Kembali\n.";
			}
			notifError(message);
			removeLoader();
		}
	});
});
</script>
<style>
/* Akan Di Mix Dengan App.css (Production) */

/* Loader */
.loader {
	display:block;
	position:fixed;
	z-index:99999999;
	left:0;
	bottom:0;
	right:0;
	top:0;
	cursor: progress;
}

.loader .spinner {
	height: 100%;
	background-image:url('../../assets/@images/logo-3.png');
	background-color:transparent;
	background-repeat:no-repeat;
	background-position:center center;
	opacity:0;
	
	animation-duration:0.9s;
	animation-name:changeZooM;
	animation-iteration-count:infinite;
	animation-direction:alternate;
	animation-delay: 0.5s;
	
	-webkit-animation-duration:0.9s;
	-webkit-animation-name:changeZooM;
	-webkit-animation-iteration-count:infinite;
	-webkit-animation-direction:alternate;
	-webkit-animation-delay: 0.5s;
}

@keyframes changeZooM {
	from {
		-ms-transform:scale(0.9, 0.9);
		-webkit-transform:scale(0.9, 0.9);
		transform:scale(0.9, 0.9);
		opacity:0;
	}
	to {
		-ms-transform:scale(1.1, 1.1);
		-webkit-transform:scale(1.1, 1.1);
		transform:scale(1.1, 1.1);
		opacity:0.5;
	}
}
@-webkit-keyframes changeZooM {
	from {
		-ms-transform:scale(0.9, 0.9);
		-webkit-transform:scale(0.9, 0.9);
		transform:scale(0.9, 0.9);
	}
	to {
		-ms-transform:scale(1.1, 1.1);
		-webkit-transform:scale(1.1, 1.1);
		transform:scale(1.1, 1.1);
	}
}


ul.errsnack {
	margin:0 0 0 5px !important;
	padding:0 !important;
}
ul.errsnack li {
	margin:2px 0 0 0 !important;
	padding:0 !important;
}
.bootstrap-select .status {
	background: #f0f0f0;
	clear: both;
	color: #999;
	font-size: 11px;
	font-style: italic;
	font-weight: 500;
	line-height: 1;
	margin-bottom: -5px;
	padding: 10px 20px;
}

</style>
</head>
<body>

<div class="navbar navbar-light navbar-expand-lg">
	<button class="sidebar-toggler" type="button">
		<span class="ua-icon-sidebar-open sidebar-toggler__open"></span>
		<span class="ua-icon-alert-close sidebar-toggler__close"></span>
	</button>

	<span class="navbar-brand">
		<a href="javascript:void(0);"><img src="{{ asset('assets/verif/img/logo-dts.png') }}" width="100px" alt="" class="navbar-brand__logo"></a>
	</span>

	<span class="navbar-brand-sm">
		<a href="javascript:void(0);"><img src="{{ asset('assets/verif/img/logo-dts.png') }}" width="100px" alt="" class="navbar-brand__logo"></a>
		<span class="ua-icon-menu slide-nav-toggle"></span>
	</span>

	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse">
		<span class="ua-icon-navbar-open navbar-toggler__open"></span>
		<span class="ua-icon-alert-close navbar-toggler__close"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbar-collapse">
		<div class="navbar__menu">
			<div class="navbar__menu-side">
				<div class="navbar-search navbar__menu-search">
				</div>
			</div>
		</div>
		<div class="dropdown navbar-dropdown">
			<a class="dropdown-toggle navbar-dropdown-toggle navbar-dropdown-toggle__user" data-toggle="dropdown" href="javascript:void(0);">
				<img src="{{ asset('assets/verif/img/users/user-3.png') }}" alt="" class="navbar-dropdown-toggle__user-avatar">
				<span class="navbar-dropdown__user-name">{{ Auth::user()->name }}</span>
			</a>
			<div class="dropdown-menu navbar-dropdown-menu navbar-dropdown-menu__user">
				<a class="dropdown-item navbar-dropdown__item" href="javascript:void(0);">Setting</a>
				<a class="dropdown-item navbar-dropdown__item" href="{{ route('logout') }}">Sign Out</a>
			</div>
		</div>
	</div>
</div>

<div class="page-wrap">
	<div class="sidebar-section" style="width: 200px;">
		<div class="sidebar-section__scroll">
			<div>
			<div class="sidebar-section__separator">Menu</div>
			<ul class="sidebar-section-nav">
				<li class="sidebar-section-nav__item">
					<a class="sidebar-section-nav__link" href="javascript:void(0);">
						<span class="sidebar-section-nav__item-icon"><img src="{{ asset('assets/verif/img/icon/growth.png') }}" width="22px"/></span>
					<span class="sidebar-section-nav__item-text">Dashboard</span>
					</a>
				</li>
				<li class="sidebar-section-nav__item">
					<a class="sidebar-section-nav__link" href="{{ route('verif') }}">
						<span class="sidebar-section-nav__item-icon"><img src="{{ asset('assets/verif/img/icon/arsip.png') }}" width="22px"/></span>
					<span class="sidebar-section-nav__item-text">Peserta</span>
					</a>
				</li>
				<li class="sidebar-section-nav__item">
					<a class="sidebar-section-nav__link" href="javascript:void(0);">
						<span class="sidebar-section-nav__item-icon"><img src="{{ asset('assets/verif/img/icon/chart.png') }}" width="22px"/></span>
						<span class="sidebar-section-nav__item-text">Report</span>
					</a>
				</li>
				<li class="sidebar-section-nav__item">
					<a class="sidebar-section-nav__link"
					   href="{{ route('admin.setting.index') }}">
						<span class="sidebar-section-nav__item-icon">
							<img src="{{ asset('assets/verif/img/icon/setting.png') }}"
								 width="22px"/>
						</span>
						<span class="sidebar-section-nav__item-text">Setting</span>
					</a>
				</li>
				<li class="sidebar-section-nav__item">
					<a class="sidebar-section-nav__link" href="{{ route('logout') }}">
						<span class="sidebar-section-nav__item-icon"><img src="{{ asset('assets/verif/img/icon/logout.png') }}" width="22px"/></span>
						<span class="sidebar-section-nav__item-text">Logout</span>
					</a>
				</li>
			</ul>
			</div>
		</div>
	</div>

	<div class="page-content" style="margin-left: 200px;">
		<div class="container-fluid container-fh">
			@yield('content')
		</div>
	</div>
</div>
</body>
</html>
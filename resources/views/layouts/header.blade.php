<!DOCTYPE html>
<html lang="id">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Sistem Informasi Monitoring Alumni Serttifikasi - Pusbang Profesi dan Sertifikasi - Badan Litbang SDM Kementerian Komunikasi dan Informatika RI">
<meta name="keywords" content="Simonas, vacancy, lowongan, lowongan kerja, startup, career, karir, Kementerian, kominfo, kemkominfo, komunikasi, litprof, litprofkom, kompetensi, sertifikasi kompetensi, sertifikasi, pelatihan, bimtek, bimbingan teknis, skkni, pusbang komunikasi, pusbang, literasi, sdm, badan litbang sdm, litbang, indonesia">
<meta name="author" content="SIMONAS">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="color:Background" content="#f4fbfa">
<meta property="og:url" content="{{ url('/') }}">
<meta property="og:type" content="article">
<meta property="og:title" content="SIMONAS - Sistem Informasi Monitoring Alumni Sertifikasi">
<meta property="og:description" content="SIMONAS - Sistem Informasi Monitoring Alumni Sertifikasi">
<meta property="og:site_name" content="Sistem Informasi Monitoring Alumni Serttifikasi - Pusbang Profesi dan Sertifikasi - Badan Litbang SDM Kementerian Komunikasi dan Informatika RI">

<title>@yield('title', 'SIMONAS | Sistem Informasi Monitoring Alumni Sertifikasi')</title>

<link rel="icon" type="image/x-icon" href="{{ asset('favicon.png') }}"/>
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon-16x16.png') }}">
<link rel="apple-touch-icon" href="{{ asset('apple-touch-icon.png') }}"/>
<link rel="apple-touch-icon" sizes="57x57" href="{{ asset('apple-touch-icon.png') }}"/>
<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('apple-touch-icon.png') }}"/>
<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('apple-touch-icon.png') }}"/>
<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('apple-touch-icon.png') }}"/>
<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('apple-touch-icon.png') }}"/>
<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('apple-touch-icon.png') }}"/>
<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('apple-touch-icon.png') }}"/>
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('apple-touch-icon.png') }}">

@include('layouts.partials.styles')

@include('layouts.partials.scripts')

</head>
<body>
<noscript>
	<div style="position: fixed;padding: 10px;background-color: white;width:3000px;height: 3000px;z-index: 5000">
		<img src=" {{ asset('assets/images/nojs.png') }}"
			 alt="">
        <h3>
			Untuk dapat menggunakan website ini,
			Mohon untuk mengaktifkan javascript anda
		</h3>
	</div>
</noscript>
<div id="wrapper">
<header id="header-container" class="fullwidth">
	<div id="header">
		<div class="container">
			<div class="left-side">
				<div id="logo">
					<a href="{{ url('/') }}"><img src=" {{ asset('assets/@images/logo.png') }}" alt="Logo DTS"></a>
				</div>

				<nav id="navigation">
					<ul id="responsive">
						<li><a href="/" class="dropdown-nav">Beranda</a></li>
						<li><a href="{{ route('public.jobs-list') }}">Find Jobs</a></li>
						<li><a href="{{ route('public.find-company') }}">Find Companies</a></li>
						<li style="margin-top:-2px !important">
							<a href="javascript::void(0);">FAQ</a>
							<ul class="dropdown-nav">
								<li><a href="#">For Candidates</a></li>
								<li><a href="#">For Companies</a></li>
							</ul>
						</li>
					</ul>
				</nav>
				<div class="clearfix"></div>
			</div>
			
			<div class="right-side">
				<div class="header-widget">
					<div class="header-notifications user-menu">
						@include('layouts.menu')
					</div>
				</div>
				
				<span class="mmenu-trigger">
					<button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</span>
			</div>
		</div>
	</div>
</header>
<div class="clearfix"></div>
const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//mix.js('resources/js/app.js', 'public/js')
//   .sass('resources/sass/app.scss', 'public/css');


// stylesheet
mix.styles([
  'public/assets/css/bootstrap-grid.min.css',
  'public/assets/css/confirm.min.css',
  'public/assets/css/icons.css',
  'public/assets/css/style.css',
  'public/assets/css/blue.css',
  'public/assets/css/local.css',
  'resources/css/bootstrap-datepicker.css',
], 'public/assets/css/App.css');

mix.styles([
  'public/assets/css/filepond.min.css'
], 'public/assets/css/upload.css');


//javascript
mix.scripts([
  'public/assets/js/jquery.min.js',
  'public/assets/js/jquery-migrate.min.js',
  'public/assets/js/bootstrap-slider.min.js',
  'public/assets/js/bootstrap-select.min.js',
  'public/assets/js/ajax-bootstrap-select.min.js',
  'public/assets/js/mmenu.min.js',
  'public/assets/js/counterup.min.js',
  'public/assets/js/magnific-popup.min.js',
  'public/assets/js/clipboard.min.js',
  'public/assets/js/snackbar.js',
  'public/assets/js/slick.min.js',
  'public/assets/js/confirm.min.js',
  'public/assets/js/tippy.all.min.js',
  'public/assets/js/simplebar.min.js',
  'public/assets/js/jquery.inputmask.bundle.min.js',
  'public/assets/js/inputmask.binding.min.js',
  'public/assets/js/filepond.min.js',
  'public/assets/js/filepond-plugin-file-validate-type.js',
  'public/assets/js/filepond-plugin-file-validate-size.js',
  'resources/js/datepicker/bootstrap-datepicker.js',
  'public/assets/js/custom.js',
  'public/assets/js/local.js'
], 'public/assets/js/App.js');


// admin verifikator assets

mix.scripts([
  'public/assets/admin/js/modernizr.min.js',
  'public/assets/admin/js/jquery.min.js',
  'public/assets/admin/js/bootstrap.min.js',
  'public/assets/admin/js/detect.js',
  'public/assets/admin/js/fastclick.js',
  'public/assets/admin/js/jquery.blockUI.js',
  'public/assets/admin/js/waves.js',
  'public/assets/admin/js/jquery.slimscroll.js',
  'public/assets/admin/js/jquery.scrollTo.min.js',
  'public/assets/admin/plugins/switchery/switchery.min.js',
  'public/assets/admin/plugins/select2/js/select2.min.js',
  'public/assets/admin/plugins/custombox/js/custombox.min.js',
  'public/assets/admin/js/jquery.core.js',
  'public/assets/admin/js/jquery.app.js',
  'public/assets/admin/plugins/snackbar.js',
  'resources/js/datepicker/bootstrap-datepicker.js',
  'public/assets/admin/js/helper.js',
], 'public/assets/admin/js/verif-app.js')
  .styles([
    'public/assets/admin/css/bootstrap.min.css',
    'public/assets/admin/css/core.css',
    'public/assets/admin/css/components.css',
    'public/assets/admin/css/pages.css',
    'public/assets/admin/css/menu.css',
    'public/assets/admin/css/responsive.css',
    'public/assets/admin/plugins/bootstrap-daterangepicker/daterangepicker.css',
    'public/assets/admin/plugins/switchery/switchery.min.css',
    'public/assets/admin/plugins/select2/css/select2.min.css',
    'public/assets/admin/plugins/snackbar/snackbar.css',
    'resources/css/bootstrap-datepicker.css',
  ], 'public/assets/css/verif-app.css');

// end admin verifikator assets


if (mix.inProduction()) {
  mix.version();
} else {
  mix.sourceMaps();
}